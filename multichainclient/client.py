import base64
import json
import logging
import binascii

import requests

log = logging.getLogger('multichain_client')

INT_MAX = 2147483647  # max count value


class Permission:
    SEND = 'send'  # to send funds, i.e. sign inputs of transactions.
    RECEIVE = 'receive'  # to receive funds, i.e. appear in the outputs of transactions.


class MultiChainException(Exception):
    class Errors:
        # Bitcoin RPC error codes
        # Values from https://github.com/MultiChain/multichain/blob/master/src/rpc/rpcprotocol.h

        #  Standard JSON - RPC 2.0 errors
        RPC_INVALID_REQUEST = -32600
        RPC_METHOD_NOT_FOUND = -32601
        RPC_INVALID_PARAMS = -32602
        RPC_INTERNAL_ERROR = -32603
        RPC_PARSE_ERROR = -32700

        # General application defined errors
        RPC_MISC_ERROR = -1  # std::exception thrown in command handling
        RPC_FORBIDDEN_BY_SAFE_MODE = -2  # Server is in safe mode, and command is not allowed in safe mode
        RPC_TYPE_ERROR = -3  # Unexpected type was passed as parameter
        RPC_INVALID_ADDRESS_OR_KEY = -5  # Invalid address or key
        RPC_OUT_OF_MEMORY = -7  # Ran out of memory during operation
        RPC_INVALID_PARAMETER = -8  # Invalid, missing or duplicate parameter
        RPC_DATABASE_ERROR = -20  # Database error
        RPC_DESERIALIZATION_ERROR = -22  # Error parsing or validating structure in raw format
        RPC_VERIFY_ERROR = -25  # General error during transaction or block submission
        RPC_VERIFY_REJECTED = -26  # Transaction or block was rejected by network rules
        RPC_VERIFY_ALREADY_IN_CHAIN = -27  # Transaction already in chain
        RPC_IN_WARMUP = -28  # Client still warming up

        # Aliases for backward compatibility
        RPC_TRANSACTION_ERROR = RPC_VERIFY_ERROR
        RPC_TRANSACTION_REJECTED = RPC_VERIFY_REJECTED
        RPC_TRANSACTION_ALREADY_IN_CHAIN = RPC_VERIFY_ALREADY_IN_CHAIN

        # P2P client errors
        RPC_CLIENT_NOT_CONNECTED = -9  # Bitcoin is not connected
        RPC_CLIENT_IN_INITIAL_DOWNLOAD = -10  # Still downloading initial blocks
        RPC_CLIENT_NODE_ALREADY_ADDED = -23  # Node is already added
        RPC_CLIENT_NODE_NOT_ADDED = -24  # Node has not been added before

        # Wallet errors
        RPC_WALLET_ERROR = -4  # Unspecified problem with wallet (key not found etc.)
        RPC_WALLET_INSUFFICIENT_FUNDS = -6  # Not enough funds in wallet or account
        RPC_WALLET_INVALID_ACCOUNT_NAME = -11  # Invalid account name
        RPC_WALLET_KEYPOOL_RAN_OUT = -12  # Keypool ran out, call keypoolrefill first
        RPC_WALLET_UNLOCK_NEEDED = -13  # Enter the wallet passphrase with walletpassphrase first
        RPC_WALLET_PASSPHRASE_INCORRECT = -14  # The wallet passphrase entered was incorrect
        RPC_WALLET_WRONG_ENC_STATE = -15  # Command given in wrong wallet encryption state (encrypting an encrypted wallet etc.)
        RPC_WALLET_ENCRYPTION_FAILED = -16  # Failed to encrypt the wallet
        RPC_WALLET_ALREADY_UNLOCKED = -17  # Wallet is already unlocked

        # MultiChain specific errors

        RPC_NOT_ALLOWED = -701  # Given action is not allowed in current database state
        RPC_NOT_SUPPORTED = -702  # API not supported
        RPC_NOT_SUBSCRIBED = -703  # Not subscribed to asset/stream
        RPC_INSUFFICIENT_PERMISSIONS = -704  # Insufficient permission
        RPC_DUPLICATE_NAME = -705  # Entity with this name already found
        RPC_UNCONFIRMED_ENTITY = -706  # Unconfirmed entity
        RPC_EXCHANGE_ERROR = -707  # Invalid exchange or error in one of exchange components
        RPC_ENTITY_NOT_FOUND = -708  # Entity with specified identifier not found
        RPC_WALLET_ADDRESS_NOT_FOUND = -709  # Address not found in the wallet
        RPC_TX_NOT_FOUND = -710  # tx not found
        RPC_BLOCK_NOT_FOUND = -711  # block not found
        RPC_OUTPUT_NOT_FOUND = -712  # Output not found in UTXO database and mempool
        RPC_OUTPUT_NOT_DATA = -713  # Output doesn't contain data
        RPC_INPUTS_NOT_MINE = -714  # Inputs in transaction don't belong to this wallet
        RPC_WALLET_OUTPUT_NOT_FOUND = -715  # Output not found in the wallet
        RPC_WALLET_NO_UNSPENT_OUTPUTS = -716  # No unspent outputs in this wallet
        RPC_GENERAL_FILE_ERROR = -717  # General file error
        RPC_UPGRADE_REQUIRED = -718  # Upgrade required

    def __init__(self, request_id, code, message):
        self.request_id = request_id
        self.code = code
        self.message = message
        super(MultiChainException, self).__init__(message)

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.request_id, self.code, self.message)


class MultiChainClient:
    __id_counter = 0
    _NO_PROXY = {"http": None, "https": None}

    def __init__(self, username, password, chain_name, host, port, secure=False, disable_proxies=True):
        self.username = username
        self.password = password
        self.chain_name = chain_name
        self.host = host
        self.port = port
        self.secure = secure
        self.disable_proxies = disable_proxies

    def create_address(self):
        """
        Returns a new address whose private key is added to the wallet.
        :return: str - the new address (ex: 19WfnPm2yPWWsCuMUS2rB6YjQhkwyPo1tUpPA5)
        """
        return self._invoke('getnewaddress')

    def list_addresses(self, addresses=None, count=None, start=None, verbose=False):
        """
        Returns information about the addresses in the wallet.
        Use count and start to retrieve part of the list only, with negative start values (like the default)
        indicating the most recently created addresses.
        Note: An invalid or non-existing address in the list will cause the whole query to fail
        :param addresses: list of addresses to lookup, None for all
        :param count: max number of addresses to return
        :param start: elements to skip from result
        :param verbose: get more information about each address
        :return: list of addresses:
            example:
            [
                 {
                  "address": "1TTroyiQN2UKTkcY1iEedJ4HaH71Xs4GYAVfhD",
                  "ismine": true,
                  "iswatchonly": false,
                  "isscript": false,
                  "pubkey": "0346474f220fae395d6b40e8092e3a5f351508db5d1d0d844e24151a69054b1d95",
                  "iscompressed": true,
                  "account": "",
                  "synchronized": true
                 }
            ]
        """
        args = []

        if not addresses:
            addresses = '*'

        if type(addresses) is list:
            # remove duplicates
            addresses = list(set(addresses))
            count = len(addresses)

        if addresses:
            if addresses == '*':
                count = INT_MAX

            args.append(addresses)

        args.append(verbose)

        if count:
            args.append(count)

        if start:
            args.append(start)

        return self._invoke('listaddresses', *args)

    def get_address_balances(self, addresses, assets=None):
        """
        Returns a list of all the asset balances for address
        :param addresses: str or list - addresses
        :param assets: str or list - assets to filter by
        :return: list - list of assets and quantities
            example:
            {
                "1TTrDxDQN2UKTkcY1iEedJ4PpPPpPpppGYAVfhD" : [
                    {
                        "name" : "Asset 1",
                        "assetref" : "140-266-31142",
                        "qty" : 1000.00000000
                    },
                    {
                        "name" : "Asset 2",
                        "assetref" : "151-266-18712",
                        "qty" : 1200.00000000
                    }
                ],
                "total" : [
                    {
                        "name" : "Asset 1",
                        "assetref" : "140-266-31142",
                        "qty" : 1000.00000000
                    },
                    {
                        "name" : "Asset 2",
                        "assetref" : "151-266-18712",
                        "qty" : 1200.00000000
                    }
                ]
            }

        """

        if addresses:
            if type(addresses) is str:
                addresses = [addresses]

        if type(addresses) is not list:
            raise ValueError('Please specify the address(s) to lookup, it must be either a string or a list of strings')

        if assets:
            if type(assets) is str:
                assets = [assets]

            if type(assets) is not list:
                raise ValueError('The assets parameter must be a list of strings')

        args = [','.join(addresses)]

        if assets:
            args.append(assets)

        return self._invoke('getmultibalances', *args)

    def list_transactions(self, address, count=None, skip=None, verbose=False):
        """
        Lists information about the count most recent transactions related to address in the wallet,
        including how they affected that address’s balance.
        :param address: str - the address
        :param count: int - N most recent transactions
        :param skip: int - use skip to go back further in history
        :param verbose: bool - provide details of transaction inputs and outputs
        :return:
        """

        args = [
            address,
            count or INT_MAX,
            skip or 0,
            verbose
        ]

        return self._invoke('listaddresstransactions', *args)

    def grant_permissions(self, admin_address, to_address, permissions=None):
        """
        Grants the provided permissions to the to_address, signed by admin_address
        :param admin_address: address which is granting the permissions
        :param to_address: address which will have it's permissions granted on
        :param permissions: list of permissions, use the Permission class for a list of available permissions
        :return:
        """
        if type(permissions) is not list or not permissions:
            raise ValueError('No permissions provided')

        if not to_address or not type(to_address) is str:
            raise ValueError('address parameter must be supplied and must be a string')

        args = [admin_address, to_address, ','.join(permissions)]
        return self._invoke('grantfrom', *args)

    def revoke_permissions(self, admin_address, to_address, permissions=None):
        """
        Revokes the provided permissions to the to_address, signed by admin_address
        :param admin_address: address which is revoking the permissions
        :param to_address:  address which will have it's permissions revoked
        :param permissions: list of permissions, use the Permission class for a list of available permissions
        :return:
        """
        if type(permissions) is not list or not permissions:
            raise ValueError('No permissions provided')

        if not to_address or not type(to_address) is str:
            raise ValueError('address parameter must be supplied and must be a string')

        args = [admin_address, to_address, ','.join(permissions)]
        return self._invoke('revokefrom', *args)

    def issue_asset(self, admin_address, to_address, asset_name, quantity, units=0.01, custom_fields=None):
        """
        Creates a new asset on the blockchain, sending the initial quantity units to to_address.
        :param admin_address: str - admin address issuing the asset
        :param to_address: str - address to which the asset will be assigned
        :param asset_name: str - name of the asset
        :param quantity: float - amount of the asset
        :param units: float - The smallest transactable unit is given by units, e.g. 0.01.
        :param custom_fields: dict - dictionary of descriptive fields
        :return: str - the transaction_id of the issuance transaction.
        """

        args = [
            admin_address,
            to_address,
            {
                "name": asset_name,
                "open": True
            },
            quantity,
            units,
            0  # native-currency (bitcoin) amount (We are not using the native currency
        ]

        if custom_fields:
            for k, v in custom_fields.items():
                custom_fields[k] = str(v)

            args.append(custom_fields)

        return self._invoke('issuefrom', *args)

    def issue_more(self, admin_address, to_address, asset_name, quantity, custom_fields=None):
        """
        Issues quantity additional units of asset, sending them to to_address.
        Any custom fields will be attached to the new issuance event, and not affect the original
        values (use list_assets with verbose=true to see both sets).
        :param admin_address: str - admin address issuing the asset
        :param to_address: str -address to which the asset will be assigned
        :param asset_name: str -name of the asset
        :param quantity: float - amount of the asset
        :param custom_fields: dict - dictionary of descriptive fields
        :return: str - the txid of the issuance transaction.
        """

        args = [
            admin_address,
            to_address,
            asset_name,
            quantity,
            0  # native-currency (bitcoin) amount (We are not using the native currency
        ]

        if custom_fields:
            for k, v in custom_fields.items():
                custom_fields[k] = str(v)

            args.append(custom_fields)

        log.info('issuemorefrom with data: ' + json.dumps(args))

        return self._invoke('issuemorefrom', *args)

    def send(self, from_address, to_address, asset_name, quantity, data=None):
        """
        Sends quantity of asset to to_address.
        :param from_address: str -address sending the asset
        :param to_address: str -address receiving the asset
        :param asset_name: str -name of the asset
        :param quantity: float - amount of the asset
        :param data: dict - dictionary of extra data to attach to the transaction
        :return: str - the txid of the issuance transaction.
        """

        args = [from_address, to_address]

        if not data:
            args.append(asset_name)
            args.append(quantity)
            args.append(0)  # native-currency (bitcoin) amount (We are not using the native currency
            return self._invoke('sendassetfrom', *args)

        args.append({asset_name: quantity})

        json_data = json.dumps(data)
        hex_data = binascii.hexlify(json_data.encode()).decode()
        args.append(hex_data)

        return self._invoke('sendwithdatafrom', *args)

    def _invoke(self, service, *args):
        MultiChainClient.__id_counter += 1

        schema = 'https' if self.secure else 'http'
        url = ''.join([schema, '://', self.host, ':', str(self.port)])

        data = {
            'id': MultiChainClient.__id_counter,
            'chain_name': self.chain_name,
            'method': service,
            'version': '1.1',
            'params': args
        }

        encoded = json.dumps(data)
        log.debug("Request: %s" % encoded)

        tmp = base64.encodebytes(('%s:%s' % (self.username, self.password)).encode()).decode().replace('\n', '')

        headers = {
            'Host': self.host,
            'User-Agent': 'MultiChainClient v0.1',
            'Authorization': "Basic %s" % tmp,
            'Content-type': 'application/json'
        }

        extras = {}

        if self.disable_proxies:
            r = requests.post(url, data=encoded, headers=headers, proxies=self._NO_PROXY)
        else:
            r = requests.post(url, data=encoded, headers=headers, **extras)

        if r.status_code == 200:
            log.info("Response: %s" % r.json())
            response = r.json()['result']
        else:
            log.error("Error! Status code: %s" % r.status_code)
            log.error("Text: %s" % r.text)
            log.error("Json: %s" % r.json())
            response = r.json()

        # check for errors
        if not response or type(response) in [str, list]:
            return response

        error = response.get('error', None)

        if error:
            raise MultiChainException(response.get('id'), error.get('code'), error.get('message'))

        return response
