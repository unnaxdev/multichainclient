from setuptools import setup

setup(name='multichainclient',
      version='0.1.10',
      description='MultiChain RPC Client',
      url='https://bitbucket.org/unnaxdev/multichainclient',
      author='Ramon A. Roldan',
      author_email='raroldan@unnax.com',
      license='MIT',
      packages=['multichainclient'],
      zip_safe=False,
      install_requires=[
          'requests',
      ])
